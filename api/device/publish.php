<?php
date_default_timezone_set("Asia/Shanghai");
require("../phpMQTT.php");

$server = "igeekiot.com";     // change if necessary
$port = 61613;                     // change if necessary
$username = "admin";                   // set your username
$password = "password";                   // set your password
$client_id = "phpMQTT-publisher"; // make sure this is unique for connecting to sever - you could use uniqid()

$mqtt = new phpMQTT($server, $port, $client_id);

if ($mqtt->connect(true, NULL, $username, $password)) {
	$mqtt->publish("test", "Hello World! at " . date("r"), 0);
	$mqtt->close();
} else {
    echo "Time out!\n";
}
