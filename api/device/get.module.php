<?php 
	error_reporting(E_ALL^E_NOTICE); //取消警告显示
	include $_SERVER['DOCUMENT_ROOT']."/common/fun.php";
  include "../conn.php";//https
  header('Content-type:application/json');
  date_default_timezone_set("Asia/Shanghai");
  
  /*******************************************
              获取单个模块信息
    参数：
    userid
    deviceid
    moduleid
    type模块类型
  *******************************************/

  //获取用户ID
  $userid = get_post_para('userid',true);
  $deviceid = get_post_para('deviceid',true);
  $type = get_post_para('type',true);
  $moduleid = get_post_para('moduleid',true);
 
  if (!$con)
  {
    $myArray["resault"]='fail';
    $myArray["error"]='数据库读取错误！';
    $json = json_encode($myArray,JSON_UNESCAPED_UNICODE);
    echo $json;
  }
  else
  {
    $myArray["resault"] = 'success';
    switch ($type) {
      case 'switch':
        $result = mysqli_query($con, "SELECT * FROM device_$type WHERE userid = '$userid' and deviceid = '$deviceid' and moduleid = $moduleid ");
        if($result){
          $row = mysqli_fetch_array($result);
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["state"] = $row['state'];
          $indexArray["power"] = $row['power'];
          $indexArray["voltage"] = $row['voltage'];
          $indexArray["timer"] = $row['timer'];
          $indexArray["created"] = $row['created'];
          $myArray["data"][] = $indexArray;
        }
        else
        {
          $myArray["data"][] = null;
          $myArray["resault"]='fail';
          $myArray["error"]='未找到模块！';
        }
        break;
      case 'gps':
        $result = mysqli_query($con, "SELECT * FROM device_$type WHERE userid = '$userid' and deviceid = '$deviceid' and moduleid = $moduleid");
        if($result){
          $row = mysqli_fetch_array($result); 
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["created"] = $row['created'];
          $myArray["data"][] = $indexArray;
        }
        else
        {
          $myArray["data"][] = null;
          $myArray["resault"]='fail';
          $myArray["error"]='未找到模块！';
        }
        break;
      case 'humidity':
        $result = mysqli_query($con, "SELECT * FROM device_$type WHERE userid = '$userid' and deviceid = '$deviceid'  and moduleid = $moduleid");
        if($result){
          $row = mysqli_fetch_array($result); 
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["created"] = $row['created'];
          $myArray["data"][] = $indexArray;
        }
        else
        {
          $myArray["data"][] = null;
          $myArray["resault"]='fail';
          $myArray["error"]='未找到模块！';
        }
        break;
      case 'temperature':
        $result = mysqli_query($con, "SELECT * FROM device_$type WHERE userid = '$userid' and deviceid = '$deviceid'  and moduleid = $moduleid");
        if($result){
          $row = mysqli_fetch_array($result);
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["created"] = $row['created'];
          $myArray["data"][] = $indexArray;
        }
        else
        {
          $myArray["data"][] = null;
          $myArray["resault"]='fail';
          $myArray["error"]='未找到模块！';
        }
        break;
      case 'led':
        $result = mysqli_query($con, "SELECT * FROM device_$type WHERE userid = '$userid' and deviceid = '$deviceid'  and moduleid = $moduleid");
        if($result){
          $row = mysqli_fetch_array($result);
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["state"] = $row['state'];
          $indexArray["light"] = $row['light'];
          $indexArray["color"] = $row['color'];
          $indexArray["created"] = $row['created'];
          $myArray["data"][] = $indexArray;
        }
        else
        {
          $myArray["data"][] = null;
          $myArray["resault"]='fail';
          $myArray["error"]='未找到模块！';
        }
        break;
      default:
        break;
    }
    // mysqli_close($con);
    // print_r($myArray); 
    $json = json_encode($myArray,JSON_UNESCAPED_UNICODE);
    echo $json;
  }
?>

