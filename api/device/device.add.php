<?php
  error_reporting(E_ALL^E_NOTICE); //取消警告显示
  header('Content-type:application/json');
  date_default_timezone_set("Asia/Shanghai");
  include $_SERVER['DOCUMENT_ROOT']."/common/fun.php";
  include "../conn.php";//https

  $userid = get_post_para('userid',true);
  $deviceid = get_post_para('deviceid',true);

  // 判断用户是否存在
  check_userid($userid,$con);
  $name = get_post_para('name',true);
  $pic = get_post_para('pic',true);
  
  //检查图片有效性，无效启用默认图片
  if(!@fopen( $pic, 'r' )){
    $pic = $_SERVER['DOCUMENT_ROOT']."/image/default/device.png";
  }

  // 存储设备信息
  $sql_insert = "insert into device (userid,deviceid,name,pic,online,created) values('$userid','$deviceid','$name','$pic','no',now())";
  $res_insert = mysqli_query($con,$sql_insert);
  if ($res_insert){
    $myArray["resault"] = 'success';
  } 
  else{
    $myArray["msg"]=mysqli_error($con);
    $myArray["resault"] = 'fail';
  }           

  // mysqli_close($con);
  // print_r($myArray); 
  $json = json_encode($myArray,JSON_UNESCAPED_UNICODE);
  echo $json;
?>