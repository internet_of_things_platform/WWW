<?php 
	error_reporting(E_ALL^E_NOTICE); //取消警告显示
	include $_SERVER['DOCUMENT_ROOT']."/common/fun.php";
  include "../conn.php";//https
  header('Content-type:application/json');
  date_default_timezone_set("Asia/Shanghai");

  //获取用户ID
  $userid = get_post_para('userid',true);
  $deviceid = get_post_para('deviceid',true);
  //模块数量
  $module_count = 0;
  if (!$con)
  {
    $myArray["resault"]='fail';
    $myArray["error"]='数据库读取错误！';
    $json = json_encode($myArray,JSON_UNESCAPED_UNICODE);
    echo $json;
  }
  else
  {
    //获取设备
    $result = mysqli_query($con, "SELECT * FROM device WHERE userid = '$userid' and deviceid = '$deviceid' ");
    if($result)
    {
      $myArray["resault"] = 'success';
      $row = mysqli_fetch_array($result);
      $myArray["userid"]=$row['userid'];
      $myArray["deviceid"]=$row['deviceid'];
      $myArray["name"]=$row['name'];
      $myArray["pic"]=$row['pic'];
      $myArray["online"]=$row['online'];
      $myArray["created"]=$row['created'];
      $myArray["ip"]=$row['ip'];
      $myArray["port"]=$row['port'];

      //获取开关
      $result = mysqli_query($con, "SELECT * FROM device_switch WHERE userid = '$userid' and deviceid = '$deviceid' ");
      if($result){
        while ($row = mysqli_fetch_array($result)) {
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["state"] = $row['state'];
          $indexArray["power"] = $row['power'];
          $indexArray["voltage"] = $row['voltage'];
          $indexArray["timer"] = $row['timer'];
          $indexArray["created"] = $row['created'];
          $module_count++;
          $myArray["switch"][] = $indexArray;
        }
      }
      else
      {
        $myArray["switch"][] = null;
      }
      

      //获取GPS
      $result = mysqli_query($con, "SELECT * FROM device_gps WHERE userid = '$userid' and deviceid = '$deviceid' ");
      if($result){
        while ($row = mysqli_fetch_array($result)) {
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["created"] = $row['created'];
          $module_count++;
          $myArray["gps"][] = $indexArray;
        }
      }
      else
      {
        $myArray["gps"][] = null;
      }
      
      //获取湿度
      $result = mysqli_query($con, "SELECT * FROM device_humidity WHERE userid = '$userid' and deviceid = '$deviceid' ");
      if($result){
        while ($row = mysqli_fetch_array($result)) {
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["created"] = $row['created'];
          $module_count++;
          $myArray["humidity"][] = $indexArray;
        }
      }
      else
      {
        $myArray["humidity"][] = null;
      }
      
      //获取温度
      $result = mysqli_query($con, "SELECT * FROM device_temperature WHERE userid = '$userid' and deviceid = '$deviceid' ");
      if($result){
        while ($row = mysqli_fetch_array($result)) {
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["created"] = $row['created'];
          $module_count++;
          $myArray["temperature"][] = $indexArray;
        }
      }
      else
      {
        $myArray["temperature"][] = null;
      }
      //获取LED
      $result = mysqli_query($con, "SELECT * FROM device_led WHERE userid = '$userid' and deviceid = '$deviceid' ");
      if($result){
        while ($row = mysqli_fetch_array($result)) {
          $indexArray = null;
          $indexArray["userid"] = $row['userid'];
          $indexArray["deviceid"]=$row['deviceid'];
          $indexArray["moduleid"] = $row['moduleid'];
          $indexArray["type"] = $row['type'];
          $indexArray["name"] = $row['name'];
          $indexArray["state"] = $row['state'];
          $indexArray["light"] = $row['light'];
          $indexArray["color"] = $row['color'];
          $indexArray["created"] = $row['created'];
          $module_count++;
          $myArray["led"][] = $indexArray;
        }
      }
      else
      {
        $myArray["led"][] = null;
      }
      $myArray["module_count"]=$module_count;
    }
    else
    {
      $myArray["resault"]='fail';
      $myArray["error"]='未找到设备！';
    }

      // mysqli_close($con);
      // print_r($myArray); 
      $json = json_encode($myArray,JSON_UNESCAPED_UNICODE);
      echo $json;
  }
?>

