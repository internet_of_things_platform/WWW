<?php 
  error_reporting(E_ALL^E_NOTICE); //取消警告显示
  header('Content-type:application/json');
  date_default_timezone_set("Asia/Shanghai");
  include $_SERVER['DOCUMENT_ROOT']."/common/fun.php";
  include "../conn.php";//https

  $userid = get_post_para('userid',true);
  $deviceid = get_post_para('deviceid',true);

  $sql_delete = "DELETE FROM device where userid='$userid' and deviceid = '$deviceid'";
  $res_delete = mysqli_query($con,$sql_delete);
  if ($res_delete){
    //删除所有关联子模块-switch
    $sql_delete = "DELETE FROM device_switch where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    $sql_delete = "DELETE FROM device_switch_operate_log where userid='$userid' and deviceid = '$deviceid'";
    $sql_delete = "DELETE FROM device_switch_record where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    //删除所有关联子模块-gps
    $sql_delete = "DELETE FROM device_gps where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    $sql_delete = "DELETE FROM device_gps_record where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    //删除所有关联子模块-led
    $sql_delete = "DELETE FROM device_led where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    $sql_delete = "DELETE FROM device_led_operate_log where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    //删除所有关联子模块-humidity
    $sql_delete = "DELETE FROM device_humidity where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    $sql_delete = "DELETE FROM device_humidity_record where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    //删除所有关联子模块-temperature
    $sql_delete = "DELETE FROM device_temperature where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
    $sql_delete = "DELETE FROM device_temperature_record where userid='$userid' and deviceid = '$deviceid'";
    $res_delete = mysqli_query($con,$sql_delete);
  	$myArray["resault"] = 'success';
  } 
  else{
  	$myArray["msg"]=mysqli_error($con);
  	$myArray["resault"] = 'fail';
  }
  $json = json_encode($myArray,JSON_UNESCAPED_UNICODE);
  echo $json;
?>
