-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019 年 07 月 08 日 23:15
-- 服务器版本: 5.5.53
-- PHP 版本: 5.4.45

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `test`
--

-- --------------------------------------------------------

--
-- 表的结构 `-des`
--

CREATE TABLE IF NOT EXISTS `-des` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '内容类型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `-des`
--

INSERT INTO `-des` (`id`, `content`, `type`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;color: rgb(255, 0, 0); background-color: rgb(255, 255, 0);&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: rgb(255, 0, 0); font-size: 36px; background-color: rgb(255, 255, 0);&quot;&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 24px; color: rgb(0, 176, 240); &quot;&gt;欢迎来到极客物联网，在这里您可以用最简单的方式控制家里的一切！&lt;/span&gt;&lt;/p&gt;', 'home'),
(2, '&lt;p&gt;&lt;span style=&quot;color: rgb(255, 0, 0); background-color: rgb(255, 255, 0);&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: rgb(255, 0, 0); font-size: 36px; background-color: rgb(255, 255, 0);&quot;&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 24px; color: rgb(0, 176, 240); background-color: rgb(255, 255, 0);&quot;&gt;欢迎来到极客物联网，分享您的内容！&lt;/span&gt;&lt;/p&gt;', 'find');

-- --------------------------------------------------------

--
-- 表的结构 `-online`
--

CREATE TABLE IF NOT EXISTS `-online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) NOT NULL,
  `province` varchar(64) NOT NULL,
  `addtime` int(10) NOT NULL DEFAULT '0',
  `city` varchar(64) NOT NULL,
  `user` varchar(64) NOT NULL,
  `uuid` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10783 ;

--
-- 转存表中的数据 `-online`
--

INSERT INTO `-online` (`id`, `ip`, `province`, `addtime`, `city`, `user`, `uuid`) VALUES
(10782, '1.86.246.150', '陕西', 1489463686, '西安', '游客', '1662577a-fba8-7801-c27a-c0fb394117b2');

-- --------------------------------------------------------

--
-- 表的结构 `-swiper`
--

CREATE TABLE IF NOT EXISTS `-swiper` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(300) DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=71 ;

--
-- 转存表中的数据 `-swiper`
--

INSERT INTO `-swiper` (`id`, `content`) VALUES
(67, 'http://img1.imgtn.bdimg.com/it/u=3967840311,321589414&fm=11&gp=0.jpg'),
(69, 'http://www.znhome.co/upload/images/3%2827%29.jpg'),
(70, 'http://img.jdzj.com/UserDocument/2015c/bewell006/Picture/2016429173640.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `-usersensor`
--

CREATE TABLE IF NOT EXISTS `-usersensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户名',
  `sensorid` varchar(32) DEFAULT '' COMMENT '传感器ID，全球唯一',
  `pic` varchar(200) DEFAULT '' COMMENT '图片',
  `name` varchar(30) DEFAULT NULL COMMENT '传感器名称',
  `online` varchar(20) NOT NULL DEFAULT '离线',
  `created` datetime NOT NULL,
  `childnumber` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- 表的结构 `-usersensorchild`
--

CREATE TABLE IF NOT EXISTS `-usersensorchild` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户名',
  `sensorid` varchar(32) DEFAULT NULL COMMENT '传感器ID，全球唯一',
  `childid` varchar(200) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL COMMENT '传感器名称',
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `-usersensordata`
--

CREATE TABLE IF NOT EXISTS `-usersensordata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户名',
  `sensorid` varchar(32) DEFAULT NULL COMMENT '传感器ID，全球唯一',
  `childid` varchar(200) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `data` datetime NOT NULL COMMENT '数据',
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `-userswitch`
--

CREATE TABLE IF NOT EXISTS `-userswitch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL,
  `switchid` varchar(32) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `pic` varchar(200) DEFAULT '' COMMENT '设备图片',
  `online` varchar(20) NOT NULL DEFAULT '离线',
  `created` datetime NOT NULL,
  `childnumber` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `-userswitchchild`
--

CREATE TABLE IF NOT EXISTS `-userswitchchild` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL,
  `switchid` varchar(32) DEFAULT NULL,
  `childid` varchar(45) DEFAULT NULL COMMENT '子id',
  `childname` varchar(200) DEFAULT NULL COMMENT '设备图片',
  `state` varchar(10) NOT NULL DEFAULT 'off' COMMENT '状态，off或on',
  `latest` datetime NOT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `dates` datetime DEFAULT NULL COMMENT '发表时间',
  `contents` text COMMENT '内容',
  `hits` int(11) DEFAULT '0' COMMENT '点击量',
  `userid` varchar(50) DEFAULT NULL COMMENT '作者',
  `picture` varchar(200) DEFAULT NULL,
  `classify` varchar(50) NOT NULL,
  `blogid` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- 转存表中的数据 `blog`
--

INSERT INTO `blog` (`id`, `title`, `dates`, `contents`, `hits`, `userid`, `picture`, `classify`, `blogid`) VALUES
(73, '测试博客001', '2019-04-20 00:01:20', '<p></p><p></p><p>2147483647</p><p>1555681653980</p><h2><span style="background-color: rgb(255, 255, 0);"><font face="Verdana">增加了一段话</font></span></h2><h2><font face="Verdana"><span style="background-color: rgb(255, 255, 0);">在增加一句话！！！</span></font></h2><p></p><p></p>', 88, '1555609230630', NULL, 'Stm32', '1555681653980');

-- --------------------------------------------------------

--
-- 表的结构 `bloganswer`
--

CREATE TABLE IF NOT EXISTS `bloganswer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `contents` text COMMENT '回复内容',
  `dates` datetime DEFAULT NULL COMMENT '回复时间',
  `toid` varchar(32) DEFAULT NULL COMMENT '回复对象',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `bloganswer`
--

INSERT INTO `bloganswer` (`id`, `userid`, `contents`, `dates`, `toid`) VALUES
(0, '1555609230630', '回复测试001<img src="http://47.102.150.235/frame/layui-master/src/images/face/52.gif" alt="[ok]">', '2019-04-19 23:34:00', '1555681653980'),
(1, '1555609230630', '<p>回复测试002<img src="http://47.102.150.235/frame/layui-master/src/images/face/57.gif" alt="[来]"></p>', '2019-04-19 23:50:13', '1555681653980'),
(2, '1555609230630', '回复测试003<img src="http://47.102.150.235/frame/layui-master/src/images/face/36.gif" alt="[酷]">', '2019-04-19 23:50:28', '1555681653980');

-- --------------------------------------------------------

--
-- 表的结构 `blog_collect`
--

CREATE TABLE IF NOT EXISTS `blog_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `blogid` varchar(32) DEFAULT NULL COMMENT '收藏帖子的ID',
  `dates` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='所有用户收藏的贴子' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `blog_zan`
--

CREATE TABLE IF NOT EXISTS `blog_zan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `blogid` varchar(32) DEFAULT NULL COMMENT '回复内容',
  `dates` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `name` varchar(45) DEFAULT NULL COMMENT '设备名称',
  `pic` varchar(200) DEFAULT NULL COMMENT '设备图片',
  `online` varchar(10) NOT NULL DEFAULT 'no' COMMENT '是否在线 【yes/no】',
  `created` datetime NOT NULL COMMENT '创建时间',
  `ip` varchar(20) NOT NULL COMMENT '设备外网ip',
  `port` varchar(10) NOT NULL COMMENT '设备端口号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='设备表' AUTO_INCREMENT=24 ;

--
-- 转存表中的数据 `device`
--

INSERT INTO `device` (`id`, `userid`, `deviceid`, `name`, `pic`, `online`, `created`, `ip`, `port`) VALUES
(17, '1557381977566', '1557381991092', '空开', 'http://47.102.150.235/image/default/device.png', 'no', '2019-05-09 14:06:45', '', ''),
(19, '1555609230630', '5d2ff3930324e4e43247118', '开关控制设备', 'http://47.102.150.235/image/default/device.png', 'no', '2019-06-05 11:37:07', '', ''),
(21, '1555609230630', '5d9ff3930324e4e43247710', '缤纷新城8路开关', 'http://igeekiot.com/image/default/device.png', 'no', '2019-06-09 21:50:09', '', ''),
(22, '1555609230630', '3500243137471031373832', 'ESP8266', 'http://47.102.150.235/image/default/device.png', 'no', '2019-06-18 16:36:58', '', ''),
(23, '1561289144189', 'asdf', '客厅灯', 'http://localhost/image/default/device.png', 'no', '2019-06-23 19:26:11', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `device_gps`
--

CREATE TABLE IF NOT EXISTS `device_gps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `deviceid` varchar(32) NOT NULL DEFAULT '' COMMENT '设备id',
  `moduleid` int(10) NOT NULL COMMENT '模块id',
  `type` varchar(20) NOT NULL DEFAULT 'gps' COMMENT '模块类型',
  `name` varchar(100) DEFAULT '坐标' COMMENT '模块名称',
  `created` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `device_gps`
--

INSERT INTO `device_gps` (`id`, `userid`, `deviceid`, `moduleid`, `type`, `name`, `created`) VALUES
(1, '1', '1', 2, 'gps', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `device_gps_record`
--

CREATE TABLE IF NOT EXISTS `device_gps_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `moduleid` int(10) DEFAULT NULL COMMENT '模块id',
  `datetime` datetime DEFAULT NULL COMMENT '时间',
  `longitude` varchar(50) NOT NULL COMMENT '精度',
  `latitude` varchar(50) NOT NULL COMMENT '纬度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_humidity`
--

CREATE TABLE IF NOT EXISTS `device_humidity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `deviceid` varchar(32) NOT NULL DEFAULT '' COMMENT '设备id',
  `moduleid` int(10) NOT NULL COMMENT '模块id',
  `type` varchar(20) NOT NULL DEFAULT 'humidity' COMMENT '模块类型',
  `name` varchar(100) DEFAULT '湿度' COMMENT '模块名称',
  `created` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_humidity_record`
--

CREATE TABLE IF NOT EXISTS `device_humidity_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `moduleid` int(10) DEFAULT NULL COMMENT '模块id',
  `datetime` datetime DEFAULT NULL COMMENT '时间',
  `humidity` varchar(20) NOT NULL COMMENT '湿度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_led`
--

CREATE TABLE IF NOT EXISTS `device_led` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `deviceid` varchar(32) NOT NULL DEFAULT '' COMMENT '设备id',
  `moduleid` int(10) NOT NULL COMMENT '模块id',
  `type` varchar(20) NOT NULL DEFAULT 'led' COMMENT '模块类型',
  `name` varchar(100) DEFAULT '灯带' COMMENT '模块名称',
  `state` varchar(10) DEFAULT '' COMMENT '开关状态 【off/on】',
  `light` varchar(20) DEFAULT '' COMMENT '亮度【0-100%】',
  `color` varchar(20) DEFAULT '' COMMENT '颜色【#000000】',
  `created` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_led_operate_log`
--

CREATE TABLE IF NOT EXISTS `device_led_operate_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `moduleid` int(10) DEFAULT NULL COMMENT '模块id',
  `datetime` datetime DEFAULT NULL COMMENT '时间',
  `action` varchar(10) NOT NULL COMMENT '动作【off/on】',
  `actionfrom` varchar(20) NOT NULL COMMENT '操作源【API/定时器/设备自带按钮】',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='开关操作记录' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_switch`
--

CREATE TABLE IF NOT EXISTS `device_switch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `deviceid` varchar(32) NOT NULL DEFAULT '' COMMENT '设备id',
  `moduleid` int(10) NOT NULL COMMENT '模块id',
  `type` varchar(20) NOT NULL DEFAULT 'switch' COMMENT '模块类型',
  `name` varchar(100) DEFAULT '开关' COMMENT '模块名称',
  `state` varchar(10) DEFAULT NULL COMMENT '开关状态 【off/on】',
  `power` varchar(20) DEFAULT NULL COMMENT '当前功率',
  `voltage` varchar(10) DEFAULT NULL COMMENT '当前电压',
  `timer` varchar(200) DEFAULT NULL COMMENT '定时',
  `created` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- 转存表中的数据 `device_switch`
--

INSERT INTO `device_switch` (`id`, `userid`, `deviceid`, `moduleid`, `type`, `name`, `state`, `power`, `voltage`, `timer`, `created`) VALUES
(25, '1557381977566', '1557381991092', 1, 'switch', '客厅灯', '', '', '', NULL, '2019-05-09 14:07:03'),
(35, '1555609230630', '5d2ff3930324e4e43247118', 1, 'switch', '设备1', NULL, NULL, NULL, '{"moduleid":1,"repeat":"everyDay","openTime":"null","closeTime":"17:30"}', '2019-06-05 11:39:04'),
(36, '1555609230630', '5d2ff3930324e4e43247118', 2, 'switch', '设备2', NULL, NULL, NULL, NULL, '2019-06-05 11:39:39'),
(37, '1555609230630', '5d2ff3930324e4e43247118', 3, 'switch', '设备3', NULL, NULL, NULL, NULL, '2019-06-05 11:47:20'),
(38, '1555609230630', '5d2ff3930324e4e43247118', 4, 'switch', '设备4', NULL, NULL, NULL, NULL, '2019-06-05 11:47:46'),
(39, '1555609230630', '5d2ff3930324e4e43247118', 5, 'switch', '设备5', NULL, NULL, NULL, NULL, '2019-06-05 11:48:02'),
(40, '1555609230630', '5d2ff3930324e4e43247118', 6, 'switch', '设备6', NULL, NULL, NULL, NULL, '2019-06-05 11:48:10'),
(41, '1555609230630', '5d2ff3930324e4e43247118', 7, 'switch', '设备7', NULL, NULL, NULL, NULL, '2019-06-05 11:48:19'),
(42, '1555609230630', '5d2ff3930324e4e43247118', 8, 'switch', '设备8', NULL, NULL, NULL, NULL, '2019-06-05 11:48:27'),
(43, '1555609230630', '5d9ff3930324e4e43247710', 1, 'switch', '开关1', NULL, NULL, NULL, NULL, '2019-06-09 21:50:29'),
(44, '1555609230630', '5d9ff3930324e4e43247710', 2, 'switch', '开关2', NULL, NULL, NULL, NULL, '2019-06-09 21:50:39'),
(45, '1555609230630', '5d9ff3930324e4e43247710', 3, 'switch', '开关3', NULL, NULL, NULL, NULL, '2019-06-09 21:50:47'),
(46, '1555609230630', '5d9ff3930324e4e43247710', 4, 'switch', '开关4', NULL, NULL, NULL, NULL, '2019-06-09 21:50:56'),
(47, '1555609230630', '5d9ff3930324e4e43247710', 5, 'switch', '开关5', NULL, NULL, NULL, NULL, '2019-06-09 21:51:11'),
(48, '1555609230630', '5d9ff3930324e4e43247710', 6, 'switch', '开关6', NULL, NULL, NULL, NULL, '2019-06-09 21:51:19'),
(49, '1555609230630', '5d9ff3930324e4e43247710', 7, 'switch', '开关7', NULL, NULL, NULL, NULL, '2019-06-09 21:51:27'),
(50, '1555609230630', '5d9ff3930324e4e43247710', 8, 'switch', '开关8', NULL, NULL, NULL, NULL, '2019-06-09 21:51:36'),
(51, '1555609230630', '3500243137471031373832', 1, 'switch', 'led1', NULL, NULL, NULL, NULL, '2019-06-18 16:37:21'),
(52, '1555609230630', '3500243137471031373832', 2, 'switch', 'led2', NULL, NULL, NULL, NULL, '2019-06-18 16:37:30'),
(53, '1555609230630', '3500243137471031373832', 3, 'switch', 'led3', NULL, NULL, NULL, NULL, '2019-06-18 16:37:38'),
(54, '1555609230630', '3500243137471031373832', 4, 'switch', 'led4', NULL, NULL, NULL, NULL, '2019-06-18 16:37:50'),
(55, '1561289144189', 'asdf', 1, 'switch', '客厅灯', NULL, NULL, NULL, NULL, '2019-06-23 19:26:30');

-- --------------------------------------------------------

--
-- 表的结构 `device_switch_operate_log`
--

CREATE TABLE IF NOT EXISTS `device_switch_operate_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `moduleid` int(10) DEFAULT NULL COMMENT '模块id',
  `datetime` datetime DEFAULT NULL COMMENT '时间',
  `action` varchar(10) NOT NULL COMMENT '动作【off/on】',
  `actionfrom` varchar(20) NOT NULL COMMENT '操作源【API/定时器/设备自带按钮】',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='开关操作记录' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_switch_record`
--

CREATE TABLE IF NOT EXISTS `device_switch_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `moduleid` int(10) DEFAULT NULL COMMENT '模块id',
  `datetime` datetime DEFAULT NULL COMMENT '时间',
  `electricquantity` varchar(20) NOT NULL COMMENT '电量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `device_temperature`
--

CREATE TABLE IF NOT EXISTS `device_temperature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `deviceid` varchar(32) NOT NULL DEFAULT '' COMMENT '设备id',
  `moduleid` int(10) NOT NULL COMMENT '模块id',
  `type` varchar(20) NOT NULL DEFAULT 'temperature' COMMENT '模块类型',
  `name` varchar(100) DEFAULT '温度' COMMENT '模块名称',
  `created` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `device_temperature`
--

INSERT INTO `device_temperature` (`id`, `userid`, `deviceid`, `moduleid`, `type`, `name`, `created`) VALUES
(1, '1557381977566', '1557381991092', 1, 'temperature', '客厅灯', '2019-05-09 14:06:56');

-- --------------------------------------------------------

--
-- 表的结构 `device_temperature_record`
--

CREATE TABLE IF NOT EXISTS `device_temperature_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户id',
  `deviceid` varchar(32) DEFAULT NULL COMMENT '设备id',
  `moduleid` int(10) DEFAULT NULL COMMENT '模块id',
  `datetime` datetime DEFAULT NULL COMMENT '时间',
  `temperature` varchar(20) NOT NULL COMMENT '温度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT '' COMMENT '系统自动生成的用户id',
  `nickname` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '用户密码',
  `avatar` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `sex` varchar(1) DEFAULT '1' COMMENT '性别字段,0，女性，1，男性',
  `phonenumber` varchar(18) DEFAULT NULL COMMENT '电话号码',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `city` varchar(30) DEFAULT '火星' COMMENT '城市',
  `qq` varchar(15) DEFAULT NULL COMMENT 'qq号',
  `level` varchar(10) DEFAULT '1' COMMENT '用户等级',
  `value` int(11) DEFAULT '0' COMMENT '用户积分',
  `regtime` datetime DEFAULT NULL COMMENT '注册时间',
  `active` varchar(8) DEFAULT '0' COMMENT '激活状态',
  `signature` varchar(200) NOT NULL DEFAULT '你太懒，连签名都没有' COMMENT '签名',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `userid`, `nickname`, `password`, `avatar`, `sex`, `phonenumber`, `email`, `city`, `qq`, `level`, `value`, `regtime`, `active`, `signature`) VALUES
(63, '1555609230630', '孙毅明', '2abc6e1ef95a10f4b2e3bad4dab21900', 'http://47.102.150.235/image/avatar/1555609230630.png', '0', NULL, 'sunyiming537@126.com', '西安', NULL, '1', 0, '2019-04-19 01:40:30', '0', '用心改善生活方式！'),
(64, '1555662330659', '黑的想漂白', '361e13931575863924c0c4c5d2e6b13a', '', '1', NULL, '931683637@qq.com', ' ', NULL, '1', 0, '2019-04-19 16:25:30', '0', '你太懒，连签名都没有'),
(65, '1557381977566', 'catccc', 'd4c746f7d093d1d201ab8d53f4a8d526', '', '1', NULL, 'catccc@gmail.com', ' ', NULL, '1', 0, '2019-05-09 14:06:17', '0', '你太懒，连签名都没有'),
(66, '1557995793645', 'aaa', 'dbce85fc2e12e4c02fdab70b23b2bbdd', '', '1', NULL, '635465650@qq.com', ' ', NULL, '1', 0, '2019-05-16 16:36:34', '0', '你太懒，连签名都没有'),
(67, '1558512618340', 'gousour', '9b62dff034056ff363c15662921e99d8', '', '1', NULL, 'gousour_124@163.com', ' ', NULL, '1', 0, '2019-05-22 16:10:18', '0', '你太懒，连签名都没有'),
(68, '1561227911947', '1234', 'e0761ddac59e21d8cc18825b88ae68f4', '', '1', NULL, '463810494@qq.com', ' ', NULL, '1', 0, '2019-06-23 02:25:11', '0', '你太懒，连签名都没有'),
(69, '1561289144189', '123456', '1e55dbf412cb74d5e2c21fb6452408c7', '', '1', NULL, '463810494@qq.co', ' ', NULL, '1', 0, '2019-06-23 19:25:45', '0', '你太懒，连签名都没有');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
