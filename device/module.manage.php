<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>设备管理 | 爱极客</title>
  <meta charset="utf-8">
  <meta name="keywords" content="爱极客">
  <!-- vue -->
  <script src="https://cdn.bootcss.com/vue/2.5.3/vue.js"></script>
  <!-- layui -->
  <link rel="stylesheet" href="/frame/layui-master/src/css/layui.css">
  <link rel="stylesheet" href="/frame/layui-master/src/css/gloabal/global.css">
  <script src="../scripts/jquery-3.4.0.min.js"></script>
  <script src="../scripts/layer-v3.1.1/layer/layer.js"></script>
  <script src="/frame/layui-master/src/layui.js"></script>
  <!-- 自定义函数 -->
  <script src="/common/fun.js"></script>
  <script type="text/javascript" src="../scripts/jquery-3.4.0.min.js"></script>
</head>
<body style="background: white;padding-top: 10px;">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common/header.php'); ?>
  <div id="app" >
  	<main v-if="user.login=='true'" style="padding-left: 60px;padding-right: 60px;" >
        <h1 style="padding-top: 20px">模块管理</h1>
  	  	<!-- 开关 -->
        <table v-if="device.switch" class="layui-table lay-skin='line' lay-size='lg' ">
  		  	  <thead>
  			    <tr>
                <th style="width: 30px">序号</th>
                <th style="width: 30px">分类</th>
                <th style="width: 30px">名称</th>
                <th style="width: 30px">定时</th>
                <!-- <th style="width: 30px">延时</th> -->
                <th style="width: 30px">编辑</th>
                <th style="width: 30px">删除</th>
                <th style="width: 30px">创建时间</th>
            </tr>
  		      </thead>
  		      <tbody v-if="device != null">
              <fieldset v-if="device.switch" class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                 <legend>【开关】</legend>
              </fieldset>
  		      	<template v-for="item in device.switch">
    		    	<tr>
    		    		<td>{{item.moduleid}}</td>
    		    		<td>{{item.type}}</td>
    		    		<td>{{item.name}}</td>
                <td>
                  <div class="layui-row">
                 <!--    <div class="layui-col-xs9">
                      <div>开：5:30</div>
                      <div>关：10:10</div>
                      <div>有效期：一次、永久</div>
                      <div>星期：1,2,3,4,5</div>
                    </div> -->
                    <div class="layui-col-xs3">
                      <a class="layui-btn layui-btn-normal" :href="'setTimer.php?type='+item.type+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">设置</a>  
                    </div>
                  </div>
                </td>
              <!--   <td>
                  <a class="layui-btn layui-btn-normal" :href="'module.edit.php?type='+item.type+'&userid='+item.userid+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">设置</a>
                </td> -->
    		    		<td>
                    <a class="layui-btn layui-btn-warm" :href="'module.edit.php?type='+item.type+'&userid='+item.userid+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">编辑</a>
                </td>
    		    		<td>
                    <!-- id采用【模块类型-模块id】的模式 -->
                    <div class="layui-btn layui-btn-danger" :id="'switch-'+item.moduleid">删除</div>
                </td>
                <td>{{item.created}}</td>
    	    		</tr>
  		      	</template>
  		      </tbody>
  		  </table>
        <!-- GPS -->
        <table v-if="device.gps" class="layui-table lay-skin='line' lay-size='lg' ">
            <thead>
            <tr>
                <th style="width: 30px">序号</th>
                <th style="width: 30px">分类</th>
                <th style="width: 30px">名称</th>
                <th style="width: 30px">创建时间</th>
                <th style="width: 30px">编辑</th>
                <th style="width: 30px">删除</th>
            </tr>
            </thead>
            <tbody v-if="device != null">
              <fieldset v-if="device.gps" class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                 <legend>【GPS】</legend>
              </fieldset>
              <template v-for="item in device.gps">
              <tr>
                <td>{{item.moduleid}}</td>
                <td>{{item.type}}</td>
                <td>{{item.name}}</td>
                <td>{{item.created}}</td>
                <td>
                    <a class="layui-btn layui-btn-warm" :href="'module.edit.php?type='+item.type+'&userid='+item.userid+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">编辑</a>
                </td>
                <td>
                    <!-- id采用【模块类型-模块id】的模式 -->
                    <div class="layui-btn layui-btn-danger" :id="'gps-'+item.moduleid">删除</div>
                </td>
              </tr>
              </template>
            </tbody>
        </table>
        <!-- 【LED彩灯】 -->
        <table v-if="device.led" class="layui-table lay-skin='line' lay-size='lg' ">
            <thead>
            <tr>
                <th style="width: 30px">序号</th>
                <th style="width: 30px">分类</th>
                <th style="width: 30px">名称</th>
                <th style="width: 30px">创建时间</th>
                <th style="width: 30px">编辑</th>
                <th style="width: 30px">删除</th>
            </tr>
            </thead>
            <tbody v-if="device != null">
              <fieldset v-if="device.led"  class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                 <legend>【LED彩灯】</legend>
              </fieldset>
              <template v-for="item in device.led">
              <tr>
                <td>{{item.moduleid}}</td>
                <td>{{item.type}}</td>
                <td>{{item.name}}</td>
                <td>{{item.created}}</td>
                <td>
                    <a class="layui-btn layui-btn-warm" :href="'module.edit.php?type='+item.type+'&userid='+item.userid+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">编辑</a>
                </td>
                <td>
                    <!-- id采用【模块类型-模块id】的模式 -->
                    <div class="layui-btn layui-btn-danger" :id="'led-'+item.moduleid">删除</div>
                </td>
              </tr>
              </template>
            </tbody>
        </table>  
        <!-- 【温度传感器】 -->
        <table v-if="device.template" class="layui-table lay-skin='line' lay-size='lg' ">
            <thead>
            <tr>
                <th style="width: 30px">序号</th>
                <th style="width: 30px">分类</th>
                <th style="width: 30px">名称</th>
                <th style="width: 30px">创建时间</th>
                <th style="width: 30px">编辑</th>
                <th style="width: 30px">删除</th>
            </tr>
            </thead>
            <tbody v-if="device != null">
              <fieldset v-if="device.template" class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                 <legend>【温度传感器】</legend>
              </fieldset>
              <template v-for="item in device.temperature">
              <tr>
                <td>{{item.moduleid}}</td>
                <td>{{item.type}}</td>
                <td>{{item.name}}</td>
                <td>{{item.created}}</td>
                <td>
                    <a class="layui-btn layui-btn-warm" :href="'module.edit.php?type='+item.type+'&userid='+item.userid+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">编辑</a>
                </td>
                <td>
                    <!-- id采用【模块类型-模块id】的模式 -->
                    <div class="layui-btn layui-btn-danger" :id="'temperature-'+item.moduleid">删除</div>
                </td>
              </tr>
              </template>
            </tbody>
        </table>  
        <!-- 【湿度传感器】 -->
        <table v-if="device.humidity" class="layui-table lay-skin='line' lay-size='lg' ">
            <thead>
            <tr>
                <th style="width: 30px">序号</th>
                <th style="width: 30px">分类</th>
                <th style="width: 30px">名称</th>
                <th style="width: 30px">创建时间</th>
                <th style="width: 30px">编辑</th>
                <th style="width: 30px">删除</th>
            </tr>
            </thead>
            <tbody v-if="device != null">
              <fieldset v-if="device.humidity" class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                 <legend>【湿度传感器】</legend>
              </fieldset>
              <template v-for="item in device.humidity">
              <tr>
                <td>{{item.moduleid}}</td>
                <td>{{item.type}}</td>
                <td>{{item.name}}</td>
                <td>{{item.created}}</td>
                <td>
                    <a class="layui-btn layui-btn-warm" :href="'module.edit.php?type='+item.type+'&userid='+item.userid+'&deviceid='+item.deviceid+'&moduleid='+item.moduleid">编辑</a>
                </td>
                <td>
                    <!-- id采用【模块类型-模块id】的模式 -->
                    <div class="layui-btn layui-btn-danger" :id="'humidity-'+item.moduleid">删除</div>
                </td>
              </tr>
              </template>
            </tbody>
        </table>  		  
  		  <div style="text-align: right;padding-top: 30px;">
  		  	<a class="layui-btn" :href="'module.add.php?deviceid='+device.deviceid"> 添加模块 </a>
  			  <a class="layui-btn layui-btn-normal" href="/device/device.manage.php">返回</a>
  		  </div>
  		<br>
  	</main>
  </div>
</body>
</html>
<?php include $_SERVER ['DOCUMENT_ROOT']."/common/footer.php";?>

<script>
  // 设备列表
  var device;
  var _deviceid = getUrlParam('deviceid');
  var user;
  layui.use('element', function(){
   var element = layui.element;
  });
  //获取用户登陆信息,同步请求
  $.ajax({
  url: "/api/user/user.php",
  async: false, //同步
  success: function (res) {
      console.log('success:',res);
      user = res;
    },
    error:function (res) {
        console.log('fail:',res);
    }
  });

   //公告,如果未登录提示登陆
  if (user.login=='false') {
    layer.open({
      type: 1
      ,title: false //不显示标题栏
      ,closeBtn: false
      ,area: '300px;'
      ,shade: 0.8
      ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
      ,btn: ['前去登陆', '看看再说']
      ,btnAlign: 'c'
      ,moveType: 1 //拖拽模式，0或者1
      ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 400;">亲,进入设备管理需要登陆哦！</div>'
      ,success: function(layero){
        var btn = layero.find('.layui-layer-btn');
        btn.find('.layui-layer-btn0').attr({
          href: '/user/login.php'
          ,target: '_blank'
        });
        btn.find('.layui-layer-btn1').attr({
          href: '/index.php'
        });
      }
    });
  }
  
  // 获取模块列表
  $.ajax({
    type:'POST',
    async: false, //同步
    url: "/api/device/device.getdevice.php",
    data:{userid:user.userid,deviceid:_deviceid},
    success: function (res) {
      device  = res;
      if (device.module_count==0) {
        layer.alert('您还没有添加模块！');
    }
      console.log('device.getdevice.list:[Ok]',res);
    },
    error:function (res) {
      device = null;
      layer.alert('API地址无效！')
      console.log('device.getdevice.list:[Fail]',res);
    }
  });

  //展示到界面
 	new Vue({
	el: '#app',
	  data: {
	  	user:user,
	    device: device
	  }
	}) 
  //所有的button引起的变化
  $("body").on('click',".layui-btn-danger",function(){
    var str=$(this).attr('id');
    var strs= new Array(); //定义一数组 
    strs=str.split("-"); //字符分割 
    var _type = strs[0];
    var _id = strs[1];

    //打印引起事件的标签信息
    console.log('moduleid:', _id,'type:',_type);

    layer.msg('删除后无法恢复，确定要删除吗？', {
      time: 20000, //5s后自动关闭
      btn: ['孤意已决','朕再想想']
      ,yes: function(){
        // 发送指令并等待响应
        $.ajax({
          url: "/api/device/module.delete.php",
          async: true,
          type:"POST",
          data:{"userid":user.userid,"deviceid":_deviceid,"moduleid":_id,"type":_type},
          success: function (res) {
            console.log('success:',res);
            // 显示成功，用户确认后跳转
            if (res.resault=='success') {
                layer.alert('删除成功！', {
                  skin: 'layui-layer-molv', //样式类名
                  anim: 4, //动画类型
                  time: 5000, //5s后自动关闭
                  btn: ['好的']
                  ,yes: function(){
                    // 跳转回原来页面
                    location.href = '/device/module.manage.php?deviceid='+_deviceid;  
                  }
                });
            }
            else{
                // 显示错误信息
                layer.msg('Sorroy,删除失败!'+res.msg, {
                      time: 20000, //20s后自动关闭
                      btn: ['知道了']
                      ,yes: function(){
                        layer.closeAll();
                      }
                    });
              }
          },
          error:function (res) {
            console.log('fail:',res);
          }
        });
      }
      ,btn1:function(){
        layer.closeAll(); 
      }
    });
  }); 
</script>