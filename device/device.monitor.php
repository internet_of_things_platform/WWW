<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>设备管理 | 爱极客</title>
  <meta charset="utf-8">
  <meta name="keywords" content="爱极客">
  <!-- vue -->
  <script src="https://cdn.bootcss.com/vue/2.5.3/vue.js"></script>
  <!-- layui -->
  <link rel="stylesheet" href="/frame/layui-master/src/css/layui.css">
  <link rel="stylesheet" href="/frame/layui-master/src/css/gloabal/global.css">
  <script src="/frame/layui-master/src/layui.js"></script>
  <!-- 自定义函数 -->
  <script src="/common/fun.js"></script>
  <script type="text/javascript" src="../scripts/jquery-3.4.0.min.js"></script>
</head>
<body style="background: white;padding-top: 10px">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common/header.php'); ?>
  <div id="app">
    <main v-if="user.login=='true'" style="padding-left: 60px;padding-right: 60px;" >
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
           <legend>设备监控</legend>
        </fieldset>

        <table class="layui-table lay-skin='line' lay-size='lg' ">
            <thead>
              <tr>
                <th style="width: 30px">设备图片</th>
                <th style="width: 30px">设备名称</th>
                <th style="width: 30px">是否在线</th>
                <!-- <th style="width: 30px">IP</th>
                <th style="width: 30px">Port</th> -->
                <th style="width: 30px">进入</th>
              </tr>
            </thead>
            <tbody v-if="device != null">
              <template v-for="item in device.list">
                <tr>
                <td><img :src='item.pic' onerror="javascript:this.src='/image/default/error.jpg';" style='width: 50px;width: 50px;border-radius:5px; '></td>
                <td>{{item.name}}</td>
                <td>{{item.online}}</td>
                <td>
                  <a class="layui-btn layui-btn-warm" :href="'device.operator.php?userid='+item.userid+'&deviceid='+item.deviceid">进入</a>
                </td>
                </tr>
              </template>
            </tbody>
        </table>
        <div style="text-align: right;padding-top: 30px;">
          <a class="layui-btn layui-btn-normal" href="/index.php">首页</a>
        </div>
      <br>
    </main>
  </div>
</body>
</html>
<?php include $_SERVER ['DOCUMENT_ROOT']."/common/footer.php";?>

<script>
  // 设备列表
  var device;

  layui.use(['layer','laydate','laypage','laytpl','layedit','form','upload','tree','table','element','util','flow','carousel','code','jquery'], function(){
    var layer,laydate,laypage,laytpl,layim,layedit,form,upload,tree,table,element,util,flow,carousel,code,$,mobile;
    layer = layui.layer;
    laydate = layui.laydate;
    laypage = layui.laypage;
    laytpl = layui.laytpl;
    layedit = layui.layedit;
    form = layui.form;
    upload = layui.upload;
    tree = layui.tree;
    table = layui.table;
    element = layui.element;
    util = layui.util;
    flow = layui.flow;
    carousel = layui.carousel;
    code = layui.code;
    $  = layui.jquery;

    if (user.login=='false') {
     //公告层
      layer.open({
        type: 1
        ,title: false //不显示标题栏
        ,closeBtn: false
        ,area: '300px;'
        ,shade: 0.8
        ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
        ,btn: ['前去登陆', '看看再说']
        ,btnAlign: 'c'
        ,moveType: 1 //拖拽模式，0或者1
        ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 400;">亲,进入设备管理需要登陆哦！</div>'
        ,success: function(layero){
          var btn = layero.find('.layui-layer-btn');
          btn.find('.layui-layer-btn0').attr({
            href: '/user/login.php'
            ,target: '_blank'
          });
          btn.find('.layui-layer-btn1').attr({
            href: '/index.php'
          });
        }
      });
    }
    // 获取列表
    $.ajax({
      type:'POST',
      async: false, //同步
      url: "/api/device/device.getlist.php",
      data:{"userid":user.userid},
      success: function (res) {
        device  = res;
        if (device.num==0) {layer.alert('您还没有添加设备！');
      }
        
        console.log('get.device.list:[Ok]',res);
      },
      error:function (res) {
        device = null;
        layer.alert('API地址无效！')
        console.log('get.device.list:[Fail]',res);
      }
    });

   

    new Vue({
    el: '#app',
      data: {
        user:user,
        device: device
      }
    }) 
  });

  //所有的button引起的变化
    $("body").on('click',".layui-btn-danger",function(){
      var _id=$(this).attr('id');
      //打印引起事件的标签信息
      console.log('deviceid:', _id);

      layer.msg('删除后无法恢复，确定要删除吗？', {
        time: 20000, //5s后自动关闭
        btn: ['孤意已决','朕再想想']
        ,yes: function(){
          // 发送指令并等待响应
          $.ajax({
            url: "/api/device/device.delete.php",
            async: true,
            type:"POST",
            data:{"userid":user.userid,"deviceid":_id},
            success: function (res) {
              console.log('success:',res);
              // 显示成功，用户确认后跳转
              if (res.resault=='success') {
                  layer.alert('删除成功！', {
                    skin: 'layui-layer-molv', //样式类名
                    anim: 4, //动画类型
                    time: 5000, //5s后自动关闭
                    btn: ['好的']
                    ,yes: function(){
                      // 跳转回原来页面
                      location.href = '/device/device.manage.php';  
                    }
                  });
              }
              else{
                  // 显示错误信息
                  layer.msg('Sorroy,删除失败!'+res.msg, {
                        time: 20000, //20s后自动关闭
                        btn: ['知道了']
                        ,yes: function(){
                          layer.closeAll();
                        }
                      });
                }
            },
            error:function (res) {
              console.log('fail:',res);
            }
          });
        }
        ,btn1:function(){
          layer.closeAll(); 
        }
      });
    }); 
</script>