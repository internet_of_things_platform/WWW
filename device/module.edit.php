<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>更新开关 | 极客物联网</title>
  <meta charset="utf-8">
  <meta name="keywords" content="物联网">
  <!-- vue -->
  <script src="https://cdn.bootcss.com/vue/2.5.3/vue.js"></script>
  <!-- layui -->
  <link rel="stylesheet" href="/frame/layui-master/src/css/layui.css">
  <link rel="stylesheet" href="/frame/layui-master/src/css/gloabal/global.css">
  <script src="/frame/layui-master/src/layui.js"></script>
  <!-- QQ登录 -->
  <script type="text/javascript" src="http://qzonestyle.gtimg.cn/qzone/openapi/qc_loader.js"></script>
  <!-- 自定义函数 -->
  <script src="/common/fun.js"></script>
  <!-- 引入 Bootstrap -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common/header.php'); ?>
  <div >
    <form class="form-horizontal" style="padding: 10% 35% 5%;">
        <script id="moduel" type="text/html">

        <div class="form-group">
            <label class="col-sm-3 control-label">名称:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="name" value="{{d.data[0].name}}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-10">
                <div class="btn btn-default" id="btn-update">提交</div>
                <a class="btn btn-default" href="/device/module.manage.php?deviceid={{d.data[0].deviceid}}">取消</a>
            </div>
        </div>
      </script>
    <!-- 建立视图。用于呈现模板渲染结果。 -->
    <div id="view"></div>
    </form>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/common/footer.php') ?>
  </body>
</html>

<script type="text/javascript">
  var _deviceid = getUrlParam('deviceid');
  var _moduleid = getUrlParam('moduleid');
  var _type = getUrlParam('type');
  layui.use(['layer','laydate','laypage','laytpl','layedit','form','upload','tree','table','element','util','flow','carousel','code','jquery'], function(){
      var layer,laydate,laypage,laytpl,layim,layedit,form,upload,tree,table,element,util,flow,carousel,code,$,mobile;
      layer = layui.layer;
      laydate = layui.laydate;
      laypage = layui.laypage;
      laytpl = layui.laytpl;
      layedit = layui.layedit;
      form = layui.form;
      upload = layui.upload;
      tree = layui.tree;
      table = layui.table;
      element = layui.element;
      util = layui.util;
      flow = layui.flow;
      carousel = layui.carousel;
      code = layui.code;
      $  = layui.jquery;
    //获取模块
    $.ajax({
      url: "/api/device/get.module.php",
      async: false,
      type:"GET",
      data:{userid:user.userid, deviceid:_deviceid,moduleid:_moduleid,type:_type},
      success: function (res) {
      console.log('success:',res);
      // 显示成功，用户确认后跳转
      if (res.resault=='success') {
        // 渲染页面
        var getTpl = moduel.innerHTML;
        var view = document.getElementById('view');
        laytpl(getTpl).render(res, function(html){
          view.innerHTML = html;
        });
      }
      else{
          // 显示错误信息
          layer.msg('拉取数据失败!'+res.msg, {
                time: 20000, //20s后自动关闭
                btn: ['知道了']
                ,yes: function(){
                  layer.closeAll();
                }
              });
        }
      },
      error:function (res) {
      console.log('fail:',res);
      }
    });

    //所有的button引起的变化
    $("#btn-update").bind("click",function(){
      var name = $("#name").val();

      //打印引起事件的标签信息
      console.log('click:', this);
      // 发送指令并等待响应
      $.ajax({
        url: "/api/device/edit.module.php",
        async: true,
        type:"GET",
        data:{"userid":user.userid,"deviceid":_deviceid,"name":name,"moduleid":_moduleid,type:_type},
        success: function (res) {
          console.log('success:',res);
          // 显示成功，用户确认后跳转
          if (res.resault=='success') {
                layer.alert('更新成功！', {
                skin: 'layui-layer-molv', //样式类名
                anim: 4, //动画类型
                time: 5000, //5s后自动关闭
                btn: ['好的']
                ,yes: function(){
                  // 跳转回原来页面
                  location.href = '/device/module.manage.php?deviceid='+_deviceid;  
                }
              });
          }
          else{
              // 显示错误信息
              layer.msg('Sorroy,更新失败!'+res.msg, {
                    time: 20000, //20s后自动关闭
                    btn: ['知道了']
                    ,yes: function(){
                      layer.closeAll();
                    }
                  });
            }
        },
        error:function (res) {
          console.log('fail:',res);
        }
      });
    }); 
  });
</script>

